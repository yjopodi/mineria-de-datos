# Instalación de las librerías a utilizar
  install.packages("RODBC")
  install.packages("ggplot2")
  install.packages("dplyr")
  getOption("repos")
# Librerías a utilizar
  library(RODBC)
  library(ggplot2)
  library(dplyr)

# Definicion de variables para el uso posterior en la conexion
  servidor="lab3"
  usuario="system"
  contrasena="system"
  
#4 Uso de las funcion odbcConnect para la conecxion a la base de datos
  conSQLServer<-odbcConnect(servidor,uid = usuario,pwd = contrasena)
  
#5 Uso de la funcion sqlQuery para importe de tablas(cliente, pedido, y detalle_pedido)
  cliente<-sqlQuery(conSQLServer,"SELECT * FROM cliente")
  pedido<-sqlQuery(conSQLServer,"SELECT * FROM pedido")
  detalle_pedido<-sqlQuery(conSQLServer,"SELECT * FROM detalle_pedido")
  
#5 Creación de vistas
  View(cliente)
  View(pedido)
  View(detalle_pedido)

#6 Unificación de los tres conjuntos de datos con un data.frame con las funciones(RBIND LEFT_JOIN O INNER_jOIN)
  union_tablas<- inner_join(pedido, detalle_pedido, by="codigo_pedido")%>%inner_join(cliente, by="codigo_cliente")
  View(union_tablas)

#7 Exploración de datos utilizando las funciones (glimse,str, str(), head(), tail(), class(),summary(),which(),names(), colnames())
  
  glimpse(cliente)
  str(detalle_pedido)
  head(detalle_pedido,3) 
  tail(pedido)
  class(cliente)
  summary(pedido)
  which(is.na(pedido))
  names(detalle_pedido)
  colnames(cliente)
  

#8 Conversión de variables (texto a factor y las fechas a date, cantidad a entero y el precio_unidad a double)
  glimpse(cliente)
  glimpse(pedido)  
  glimpse(detalle_pedido)
  pedido$ESTADO<-as.factor(pedido$ESTADO)
  pedido$COMENTARIOS<-as.factor(pedido$COMENTARIOS)
  pedido$FECHA_PEDIDO<-as.Date(pedido$FECHA_PEDIDO)
  pedido$FECHA_ESPERADA<-as.Date(pedido$FECHA_ESPERADA)
  pedido$FECHA_ENTREGA<-as.Date(pedido$FECHA_ENTREGA)
  detalle_pedido$CANTIDAD<-as.integer(detalle_pedido$CANTIDAD)
  detalle_pedido$PRECIO_UNIDAD<-as.double(detalle_pedido$PRECIO_UNIDAD)

#9 Frecuencia de los datos de tipo cadena.
  Frec<-table(cliente$NOMBRE_CLIENTE, cliente$PAIS)
  
#10 Determine en cuales columnas hay valores nulos o ausentes
  apply(is.na(union_tablas),MARGIN = 2 , FUN = sum)
  
#11 Función boxplot para graficar si existen valores atípicos en las variables del conjunto de datos
  boxplot(union_tablas $CANTIDAD,
          horizontal = TRUE,
          lwd = 2, 
          col =  rgb(0.5, 0, 1, alpha = 0.5),
          xlab = "CANTIDAD", 
          main = "BOXPLOT - CANTIDAD",
          notch = TRUE, 
          border = "black",
          outpch = 25, 
          outbg = "blue", 
          whiskcol = "black", 
          whisklty = 2,
          lty = 1)
  
#12 Función hist para determinar la distribución de los datos de total_ventas
  hist(union_tablas $total_ventas,
       main = "HISTOGRAMA - TOTAL VENTAS",
       ylab="Frecuencia",
       xlab="Ventas",
       col="chocolate",
       las=0,
       freq=TRUE
  )
  
#13  Utilizacion e implementacion de funcion vista en clase para eliminar los NA y los valores 
#  atípicos en las variables de Precio y Cantidad
  
    union_tablas<-na.omit(union_tablas)
    
#14 Creación de un conjunto de datos nuevo, con las columnas (nombre_cliente,ciudad, 
#  región,país,fecha_pedido,estado, cantidad, precio_unidad,código_producto)
  
  var_nueva<-sqlQuery(conSQLServer,"SELECT c.nombre_cliente,c.ciudad,c.region,c.pais,p.fecha_pedido,p.estado,dp.cantidad,dp.precio_unidad,dp.codigo_producto FROM cliente c INNER JOIN pedido p ON c.codigo_cliente = p.codigo_cliente INNER JOIN detalle_pedido dp ON dp.CODIGO_PEDIDO = p.CODIGO_PEDIDO")
  
#15 En el nuevo conjunto de datos genere una nueva columna de nombre total_ventas la cual será el producto de multiplicar la cantidad * precio_unidad
  var_nueva$total_ventas<-var_nueva$CANTIDAD*var_nueva$PRECIO_UNIDAD
  
#16 Cree dos nuevas columnas y extraiga el mes y el año a partir de la fecha_pedido
  var_nueva<-mutate(var_nueva,month=as.numeric(format(var_nueva$FECHA_PEDIDO,'%m')))
  var_nueva<-mutate(var_nueva,year=as.numeric(format(var_nueva$FECHA_PEDIDO,'%Y')))

#17 Grafique con ggplot las ventas por ciudad utilice la utilidad %>%
  
  
#18 Grafique las ventas por año y excluya los datos del 2006, utilice la utilidad %>% y la función filter.
  
  
#19 Grafique las Cantidad de clientes por pais, utilice la utilidad %>% con la función summarize.
  

#20  Cree un nuevo juego de datos con nombre ventas por país y producto utilice la utilidad %>% y con la función summarize
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  